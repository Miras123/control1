const fet = require('node-fetch');// Модуль чтения  HTML страниц 
const file = require('fs');// Модуль работы с файловой системой

fet('https://ru.wikipedia.org/wiki/TypeScript') // Считывание данных
    .then(res => res.text())
    .then(body => { 
        file.writeFileSync('control.txt', body);
});



let data = file.readFileSync('control.txt'); 
data = data.toString(); 
data = data.split(/<[^>]+>/g); 

let text = "";


data.forEach(function (piece) {

    
    piece = piece.trim();

    
    if (piece.length) {
        text = text + ' ' + piece;
    }
});

// String обязательных букв по которым идет проверка.
const requiredChars = 'abcdefghijklmnopqrstuvwxyz' + 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя';

const duplicatedTwoChar = []; // Все повторяющиеся трехбуквенные слова.
const uniqueTwoChar = []; // Уникальные трехбуквенные слова.

// Разбиваем общий текст через пробелы.
var textnew = text.split(' ');

// Итерируем array.
textnew.forEach(function(piece) {

    // Если слово (стринг) длинною в два символа и при этом первый и второй символ есть в string-е обязательных букв.
    if (piece.length === 3 && requiredChars.indexOf(piece[0]) !== -1 && requiredChars.indexOf(piece[1]) !== -1 && requiredChars.indexOf(piece[2]) !== -1) {

        // Добавляем слово в список повторяющихся слов.
        duplicatedTwoChar.push(piece);

        // Если слово отсутствует в списке уникальных слов добавляем, если нет то пропускаем.
        if (uniqueTwoChar.indexOf(piece) === -1) {
            uniqueTwoChar.push(piece);
        }
    }
});


console.log("Все трехбуквенные слова", duplicatedTwoChar);
console.log("Уникальные трехбуквенные слова", uniqueTwoChar);
console.log("Количество всех трехбуквенных слов", duplicatedTwoChar.length);
console.log("Количество уникальных трехбуквенных слов", uniqueTwoChar.length);